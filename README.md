# Photo Contest

A team of aspiring photographers want an application that can allow them to easily manage online photo contests.
The application has two main parts:
- Organizational – here, the application owners can organize photo contests.
- For photo junkies – everyone is welcome to register and to participate in contests. Junkies with certain ranking can be invited to be jury.

## Public Part
- The public part must be accessible without authentication i.e. for anonymous users.
- Landing page – you can show the latest winning photos or something else that might be compelling for people to register.
- Login form – redirects to the private area of the application. Login requires username and password.
- Register form – registers someone as a Photo Junkie. Requires username, email, first name, last name, and password.

## Private part 
- Accessible only if the user is authenticated.

## Dashboard page
- For Organizers: 
    - There must be a way to setup a new Contest.
    - There must be a way to view Contests which are in Phase I.
    - There must be a way to view Contests which are in Phase II.
    - There must be a way to view Contests which are Finished.
    - Must be able to make other users organizers.
    - Must be able to delete contest entry (due to offensive nature of the photo, comment etc.) Jury must be able to delete as well.
    - Jury must not be able to participate within the same contest where they are jury.
    - There must be a way to view Photo Junkies & Ordered by ranking.

- For Photo Junkies:
    - There must be a way to view active Open contests.
    - There must be a way to view contests that the junkie currently participates in.
    - There should be a way to view finished contests that the junkie participated in.
    - Display current points and ranking and how much until next ranking at a visible place.

## Contest page
The Contest Category is always visible.
- Phase I 
    - Remaining time until Phase II should be displayed.
    - Jury can view submitted photos but cannot rate them yet.
    - Junkies see enroll button if the contest is Open and they are not participating.
    - If they are participating and have not uploaded a photo, they see a form for upload:
        - Title – short text (required)
        - Story – long text, which tells the captivating story of the phot (required)
        - Photo – file (required)
        - Only one photo can be uploaded per participant. The photo, title, and story cannot be edited. Display a warning on submit that any data cannot be changed later.

- Phase II 
    - Remaining time until Finish phase.
    - Participants cannot upload anymore.
    - Jury sees a form for each submitted photo.
    - Score (1-10) (required)
    - Comment (long text) (required)
    - Checkbox to mark that the photo does not fit the contest category. If the checkbox is selected, score 0 is assigned automatically and a Comment that the category is wrong. This is the only way to assign Score outside the [1, 10] interval.
    - Each juror can give one review per photo, if a photo is not reviewed, a default score of 3 is awarded.

- Finished
    - Jury can no longer review photos.
    - Participants view their score and comments. 
    - In this phase, participants can also view the photos submitted by other users, along with their scores and comments by the Jury. 

## Create Contest Page
Create Contest Form  – either a new page, or on the organizer’s dashboard. The following must be easy to setup.
- Title – text field (required and unique)
- Category – text field (required)
- Open or Invitational Contest. Open means that everyone (except the jury can join)
    - If invitational – a list of users should be available, along with the option to select them.
- Phase I time limit – anything from one day to one month
- Phase II time limit – anything from one hour to one day
- Select Jury – all users with Organizer role are automatically selected
    - IAdditionally, users with ranking Photo Master can also be selected, if the organizers decide 
- Cover photo - a photo can be selected for a contest.
    - Option 1 – upload a cover photo.
    - Option 2 – paste the URL of an existing photo.
    - Option 3 – select the cover from previously uploaded photos.
- The organizer must be able to choose between all three options and select the easiest for him/her (all 3 required if cover photo is implemented)

## Scoring
Contest participation should award points. Points are accumulative, so being invited and subsequently winning will award 53 points totals.
- Joining open contest – 1 point
- Being invited by organizer – 3 points
- 3rd place – 20 points (10 points if shared 3rd)
- 2nd place – 35 points (25 points if shared 2nd)
- 1st place – 50 points (40 points if shared 1st)
- Finishing at 1st place with double the score of the 2nd (e.g., 1st has been awarded 8.6 points average, and 2nd is 4.3 or less) – 75 points
- In case of a tie, positions are shared, so there can be more than one participant at 1st, 2nd, and 3rd places, all in the same contest.

For example, two 1st places, one 2nd and four 3rds; the two winners will each get 40 points, the only 2nd place will get the full 35 points, and the four 3rd finishers will get 10 points.

## Ranking:
- (0-50) points – Junkie
- (51 – 150) points – Enthusiast
- (151 – 1000) points – Master (can now be invited as jury)
- (1001 – infinity) points – Wise and Benevolent Photo Dictator (can still be jury)

## Swagger
http://localhost:8080/swagger-ui/ 
