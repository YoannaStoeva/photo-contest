package com.example.photocontestproject.services;

import com.example.photocontestproject.exceptions.DuplicateEntityException;
import com.example.photocontestproject.exceptions.EntityNotFoundException;
import com.example.photocontestproject.models.Category;
import com.example.photocontestproject.repositories.contracts.CategoryRepository;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.jupiter.MockitoExtension;

import java.util.ArrayList;

import static com.example.photocontestproject.Helpers.*;

@ExtendWith(MockitoExtension.class)
public class CategoryServiceTest {

    @Mock
    CategoryRepository mockRepository;

    @InjectMocks
    CategoryServiceImpl categoryService;

    @Test
    public void getAll_Should_ReturnAllCategories_When_CategoryExist() {
        //Arrange
        Mockito.when(mockRepository.getAll()).thenReturn(new ArrayList<>());

        //Act
        categoryService.getAll();

        //Assert
        Mockito.verify(mockRepository, Mockito.times(1)).getAll();
    }

    @Test
    public void getById_Should_ReturnCategory_When_CategoryExist() {
        //Arrange
        Category mockCategory = createMockCategory();
        Mockito.when(mockRepository.getById(mockCategory.getId())).thenReturn(mockCategory);

        //Act
        Category result = categoryService.getById(mockCategory.getId());

        //Assert
        Assertions.assertAll(
                () -> Assertions.assertEquals(mockCategory.getId(), result.getId()),
                () -> Assertions.assertEquals(mockCategory.getName(), result.getName())
        );
    }

    @Test
    public void getById_Should_Throw_When_CategoryDoesNotExists() {
        //Arrange
        Mockito.when(mockRepository.getById(100)).thenThrow(new EntityNotFoundException());

        //Act, Assert
        Assertions.assertThrows(EntityNotFoundException.class, () -> categoryService.getById(100));
    }

    @Test
    public void getByName_Should_ReturnCategory_When_MatchExists() {
        //Arrange
        Category mockCategory = createMockCategory();
        Mockito.when(mockRepository.getByField("name", mockCategory.getName()))
                .thenReturn(mockCategory);

        //Act
        categoryService.getByName(createMockCategory().getName());

        //Assert
        Mockito.verify(mockRepository, Mockito.times(2)).getByField("name", mockCategory.getName());
    }

    @Test
    public void getByName_Should_Throw_When_CategoryDoesNotExists() {
        //Arrange
        Category mockCategory = createMockCategory();
        Mockito.when(mockRepository.getByField("name", mockCategory.getName()))
                .thenThrow(new EntityNotFoundException());

        //Act, Assert
        Assertions.assertThrows(EntityNotFoundException.class, () -> categoryService.getByName(mockCategory.getName()));
    }

    @Test
    public void create_should_throw_when_categoryWithSameNameExists() {
        //Arrange
        Category mockCategory = createMockCategory();
        Mockito.when(mockRepository.getByField("name", mockCategory.getName()))
                .thenReturn(mockCategory);

        //Act, Assert
        Assertions.assertThrows(DuplicateEntityException.class, () -> categoryService.create(createMockCategory()));
    }

    @Test
    public void create_should_callRepository_when_categoryWithSameDoesNotExists() {
        //Arrange
        Category mockCategory = createMockCategory();
        Mockito.when(mockRepository.getByField("name", mockCategory.getName()))
                .thenThrow(EntityNotFoundException.class);

        //Act
        categoryService.create(mockCategory);

        //Arrange
        Mockito.verify(mockRepository, Mockito.times(1)).create(mockCategory);
    }
}
