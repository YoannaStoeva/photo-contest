package com.example.photocontestproject.services;

import com.example.photocontestproject.exceptions.EntityNotFoundException;
import com.example.photocontestproject.exceptions.IllegalRequestException;
import com.example.photocontestproject.exceptions.InvalidContestPhaseException;
import com.example.photocontestproject.exceptions.UnauthorizedOperationException;
import com.example.photocontestproject.models.Entry;
import com.example.photocontestproject.models.Review;
import com.example.photocontestproject.models.User;
import com.example.photocontestproject.models.enums.Phase;
import com.example.photocontestproject.repositories.contracts.ReviewRepository;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.jupiter.MockitoExtension;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.Set;

import static com.example.photocontestproject.Helpers.*;
import static com.example.photocontestproject.utils.constants.GlobalConstants.DEFAULT_COMMENT;
import static com.example.photocontestproject.utils.constants.GlobalConstants.DEFAULT_SCORE;

@ExtendWith(MockitoExtension.class)
public class ReviewServiceTest {

    @Mock
    ReviewRepository reviewMockRepository;

    @InjectMocks
    ReviewServiceImpl reviewMockService;

    @Test
    public void getAll_should_callRepository() {
        //Arrange
        Mockito.when(reviewMockRepository.getAll())
                .thenReturn(new ArrayList<>());

        // Act
        reviewMockService.getAll();

        //Assert
        Mockito.verify(reviewMockRepository, Mockito.times(1))
                .getAll();
    }

    @Test
    public void getCountAllReviews_should_callRepository() {
        //Arrange
        Mockito.when(reviewMockRepository.getCountAllReviews())
                .thenReturn(1);

        // Act
        reviewMockService.getCountAllReviews();

        //Assert
        Mockito.verify(reviewMockRepository, Mockito.times(1))
                .getCountAllReviews();
    }

    @Test
    public void getById_should_returnReview_when_matchExist() {
        //Arrange
        Review mockReview = createMockReview();
        Mockito.when(reviewMockRepository.getById(mockReview.getId()))
                .thenReturn(mockReview);

        // Act
        Review review = reviewMockService.getById(mockReview.getId());

        //Assert
        Assertions.assertAll(
                () -> Assertions.assertEquals(mockReview.getId(), review.getId()),
                () -> Assertions.assertEquals(mockReview.getComment(), review.getComment()),
                () -> Assertions.assertEquals(mockReview.getEntry(), review.getEntry()),
                () -> Assertions.assertEquals(mockReview.getJuror(), review.getJuror()),
                () -> Assertions.assertEquals(mockReview.getScore(), review.getScore()));
    }

    @Test
    public void getByEntry_should_callRepository_when_matchExist() {
        //Arrange
        Review mockReview = createMockReview();
        Entry mockEntry = mockReview.getEntry();

        // Act
        reviewMockService.getByEntry(mockEntry);

        //Assert
        Mockito.verify(reviewMockRepository, Mockito.times(1))
                .getByEntry(mockEntry.getId());
    }

    @Test
    public void create_should_throw_whenContestPhaseNotSecond() {
        //Arrange
        User juror = createMockUser();
        Review mockReview = createMockReview();
        Entry reviewEntry = mockReview.getEntry();
        reviewEntry.getContest().setPhase(Phase.FIRST);

        // Act
        //Assert
        Assertions.assertThrows(InvalidContestPhaseException.class, () -> reviewMockService.create(mockReview,juror));
    }

    @Test
    public void create_should_throw_whenReviewByJurorExist() {
        //Arrange
        User juror = createMockUser();
        Review mockReview = createMockReview();
        Entry reviewEntry = mockReview.getEntry();
        reviewEntry.getContest().setPhase(Phase.SECOND);
        Mockito.when(reviewMockRepository.getByJurorAndByEntry(juror.getId(),reviewEntry.getId()))
                .thenReturn(mockReview);

        // Act
        //Assert
        Assertions.assertThrows(UnauthorizedOperationException.class, () -> reviewMockService.create(mockReview,juror));
    }


    @Test
    public void create_should_throw_whenScoreOutsideRange() {
        //Arrange
        User juror = createMockUser();
        Review mockReview = createMockReview();
        mockReview.setScore(11);
        Entry reviewEntry = mockReview.getEntry();
        reviewEntry.getContest().setPhase(Phase.SECOND);
        Mockito.when(reviewMockRepository.getByJurorAndByEntry(juror.getId(),reviewEntry.getId()))
                .thenThrow(EntityNotFoundException.class);

        // Act
        //Assert
        Assertions.assertThrows(IllegalRequestException.class, () -> reviewMockService.create(mockReview,juror));
    }

    @Test
    public void create_should_throw_whenCommentEmpty() {
        //Arrange
        User juror = createMockUser();
        Review mockReview = createMockReview();
        mockReview.setComment("");
        Entry reviewEntry = mockReview.getEntry();
        reviewEntry.getContest().setPhase(Phase.SECOND);
        Mockito.when(reviewMockRepository.getByJurorAndByEntry(juror.getId(),reviewEntry.getId()))
                .thenThrow(EntityNotFoundException.class);

        // Act
        //Assert
        Assertions.assertThrows(IllegalRequestException.class, () -> reviewMockService.create(mockReview,juror));
    }

    @Test
    public void create_should_callRepository_whenInputValid() {
        //Arrange
        User juror = createMockUser();
        Review mockReview = createMockReview();
        Entry reviewEntry = mockReview.getEntry();
        reviewEntry.getContest().setPhase(Phase.SECOND);
        Mockito.when(reviewMockRepository.getByJurorAndByEntry(juror.getId(),reviewEntry.getId()))
                .thenThrow(EntityNotFoundException.class);

        // Act
        reviewMockService.create(mockReview,juror);

        //Assert
        Mockito.verify(reviewMockRepository, Mockito.times(1))
                .create(mockReview);
    }

    @Test
    public void getEntryFinalScore_should_throw_whenContestPhaseNotFinal() {
        //Arrange
        Review mockReview = createMockReview();
        Entry reviewEntry = mockReview.getEntry();
        reviewEntry.getContest().setPhase(Phase.SECOND);

        // Act
        //Assert
        Assertions.assertThrows(InvalidContestPhaseException.class,
                () -> reviewMockService.getEntryFinalScore(reviewEntry));
    }

    @Test
    public void getEntryFinalScore_should_callRepository_whenInputValid() {
        //Arrange
        User juryOne = createMockOrganizer();
        User juryTwo = createMockUser();
        Set<User> juries = new HashSet<>();
        juries.add(juryOne);
        juries.add(juryTwo);
        Review mockReview = createMockReview();
        Entry reviewEntry = mockReview.getEntry();
        reviewEntry.getContest().setJury(juries);
        reviewEntry.getContest().setPhase(Phase.FINISHED);
        Set<Review> reviews = new HashSet<>();
        reviews.add(mockReview);
        reviewEntry.setReview(reviews);
        Mockito.when(reviewMockRepository.getEntryReviewCount(reviewEntry.getId())).
                thenReturn(2);

        // Act
        reviewMockService.getEntryFinalScore(reviewEntry);

        //Assert
        Mockito.verify(reviewMockRepository, Mockito.times(1))
                .avgEntryScore(reviewEntry);
    }

}
