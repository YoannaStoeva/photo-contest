package com.example.photocontestproject.controllers.mvc;

import com.example.photocontestproject.controllers.AuthenticationHelper;
import com.example.photocontestproject.exceptions.AuthenticationFailureException;
import com.example.photocontestproject.exceptions.EntityNotFoundException;
import com.example.photocontestproject.models.User;
import com.example.photocontestproject.services.contracts.ContestService;
import com.example.photocontestproject.services.contracts.EntryService;
import com.example.photocontestproject.services.contracts.ReviewService;
import com.example.photocontestproject.services.contracts.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;

import javax.servlet.http.HttpSession;
import java.util.Optional;

import static com.example.photocontestproject.utils.constants.GlobalConstants.ORGANIZER_ROLE;

@Controller
@RequestMapping("/")
public class HomeMVCController {
    private final AuthenticationHelper authenticationHelper;
    private final ContestService contestService;
    private final EntryService entryService;
    private final ReviewService reviewService;
    private final UserService userService;

    @Autowired
    public HomeMVCController(AuthenticationHelper authenticationHelper, ContestService contestService, EntryService entryService, ReviewService reviewService, UserService userService) {
        this.authenticationHelper = authenticationHelper;
        this.contestService = contestService;
        this.entryService = entryService;
        this.reviewService = reviewService;
        this.userService = userService;
    }

    @ModelAttribute("isAuthenticated")
    public boolean populateIsAuthenticated(HttpSession session) {
        return session.getAttribute("currentUser") != null;
    }

    @ModelAttribute("isOrganizer")
    public boolean populateIsOrganizer(HttpSession session) {
        User loggedUser;
        try {
            loggedUser = authenticationHelper.tryGetUser(session);
            return loggedUser.getRole().getRole().equals(ORGANIZER_ROLE);
        } catch (AuthenticationFailureException | EntityNotFoundException | NullPointerException e) {
            return false;
        }
    }

    @GetMapping
    public String showHomePage(Model model, Optional<String> search, HttpSession session) {
        model.addAttribute("winners", entryService.getAllWinners(contestService.getMostRecentlyFinished()));
        model.addAttribute("contests", contestService.getCountAllContests());
        model.addAttribute("entries", entryService.getCountAllEntries());
        model.addAttribute("reviews", reviewService.getCountAllReviews());
        model.addAttribute("users", userService.getCountAllRegularUsers());
        if (populateIsAuthenticated(session)) model.addAttribute("loggedUser",
                userService.getById(authenticationHelper.tryGetUser(session).getId()));
        return "index";
    }

    @GetMapping("/winners")
    public String showWinnersPage(Model model, HttpSession session) {
        User loggedUser;
        try {
            loggedUser = authenticationHelper.tryGetUser(session);
        } catch (AuthenticationFailureException e) {
            return "redirect:/auth/login";
        }
        model.addAttribute("loggedUser", loggedUser);
        model.addAttribute("winners",
                entryService.getAllWinners(contestService.getAllPhaseFinished(0,loggedUser)));
        return "winners";
    }
}