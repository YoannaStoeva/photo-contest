package com.example.photocontestproject.controllers.mvc;

import com.example.photocontestproject.controllers.AuthenticationHelper;
import com.example.photocontestproject.exceptions.AuthenticationFailureException;
import com.example.photocontestproject.exceptions.DuplicateEntityException;
import com.example.photocontestproject.exceptions.EntityNotFoundException;
import com.example.photocontestproject.exceptions.UnauthorizedOperationException;
import com.example.photocontestproject.models.*;
import com.example.photocontestproject.models.dtos.*;
import com.example.photocontestproject.models.enums.Phase;
import com.example.photocontestproject.services.contracts.*;
import com.example.photocontestproject.utils.mappers.ContestMapper;
import com.example.photocontestproject.utils.mappers.EntryMapper;
import com.example.photocontestproject.utils.mappers.InviteMapper;
import com.example.photocontestproject.utils.mappers.PhotoMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.validation.ObjectError;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import javax.servlet.http.HttpSession;
import javax.validation.Valid;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import static com.example.photocontestproject.utils.constants.GlobalConstants.ORGANIZER_ROLE;
import static com.example.photocontestproject.utils.constants.GlobalConstants.PLEASE_SELECT_A_FILE_TO_UPLOAD;

@Controller
@RequestMapping("/contests")
public class ContestMVCController {

    private final AuthenticationHelper authenticationHelper;
    private final ContestService contestService;
    private final ContestMapper contestMapper;
    private final EntryService entryService;
    private final EntryMapper entryMapper;
    private final PhotoService photoService;
    private final PhotoMapper photoMapper;
    private final CategoryService categoryService;
    private final UserService userService;
    private final InviteMapper inviteMapper;
    private final InviteService inviteService;

    @Autowired
    public ContestMVCController(AuthenticationHelper authenticationHelper, ContestService contestService,
                                ContestMapper contestMapper, EntryService entryService, EntryMapper entryMapper,
                                PhotoService photoService, PhotoMapper photoMapper, CategoryService categoryService,
                                UserService userService, InviteMapper inviteMapper,
                                InviteService inviteService) {
        this.authenticationHelper = authenticationHelper;
        this.contestService = contestService;
        this.contestMapper = contestMapper;
        this.entryService = entryService;
        this.entryMapper = entryMapper;
        this.photoService = photoService;
        this.photoMapper = photoMapper;
        this.categoryService = categoryService;
        this.userService = userService;
        this.inviteMapper = inviteMapper;
        this.inviteService = inviteService;
    }

    @ModelAttribute("isAuthenticated")
    public boolean populateIsAuthenticated(HttpSession session) {
        return session.getAttribute("currentUser") != null;
    }

    @ModelAttribute("isOrganizer")
    public boolean populateIsOrganizer(HttpSession session) {
        User loggedUser;
        try {
            loggedUser = authenticationHelper.tryGetUser(session);
            return loggedUser.getRole().getRole().equals(ORGANIZER_ROLE);
        } catch (AuthenticationFailureException | EntityNotFoundException | NullPointerException e) {
            return false;
        }
    }



    @GetMapping("/{id}")
    public String showSingleContest(@PathVariable int id, Model model, HttpSession session) {
        User loggedUser;
        try {
            loggedUser = authenticationHelper.tryGetUser(session);
        } catch (AuthenticationFailureException e) {
            return "redirect:/auth/login";
        }
        model.addAttribute("loggedUser", loggedUser);
        try {
            Contest contest = contestService.getById(id);
            if (!contest.isOpenAccess() && !inviteService.isUserInvitedOrOrganizer(loggedUser, contest))
                return "access-denied";
            try {
                model.addAttribute("userEntry", entryService.getByUserAndContestId(loggedUser, id));
            } catch (EntityNotFoundException e) {
                model.addAttribute("userEntry", new Entry());
            }
            List<Entry> entries = entryService.getAllByContestId(id);
            if (contest.getPhaseTwoExpiration().isBefore(LocalDateTime.now()) && !entries.isEmpty()) {
                model.addAttribute("winners", entryService.getWinnerOrWinners(contest).get(1));
                model.addAttribute("scores", entryService.getFinalScoresForEntriesList(entries));
            }
            model.addAttribute("entries", entries);
            model.addAttribute("contest", contest);
            model.addAttribute("isJury", userService.isUserJury(loggedUser, contest));
            model.addAttribute("contestJuryList", userService.getContestJury(contest));
            model.addAttribute("timeDifference", contestService.calculatePhaseTimeDiff(contest));
            model.addAttribute("entriesCount", entryService.getCountByContestId(id));
            return "contest";
        } catch (EntityNotFoundException e) {
            model.addAttribute("error", e.getMessage());
            return "404";
        }
    }

    @GetMapping()
    public String showDashboardPage(Model model, HttpSession session) {
        User loggedUser;
        try {
            loggedUser = authenticationHelper.tryGetUser(session);
        } catch (AuthenticationFailureException e) {
            return "redirect:/auth/login";
        }
        model.addAttribute("phaseOnes", contestService.getAllPhaseOne(6,loggedUser));
        model.addAttribute("phaseTwos", contestService.getAllPhaseTwo(6,loggedUser));
        model.addAttribute("phaseFinished", contestService.getAllPhaseFinished(6,loggedUser));
        model.addAttribute("openUserContests", contestService.getOpenByUser(loggedUser));
        model.addAttribute("finishedUserContests", contestService.getFinishedByUser(loggedUser));
        model.addAttribute("loggedUser", loggedUser);
        model.addAttribute("openContest", contestService.getAllNotFinished(loggedUser));
        return "dashboard";
    }

    @GetMapping("/new")
    public String showCreateContestPage(Model model, HttpSession session) {
        User loggedUser;
        try {
            loggedUser = authenticationHelper.verifyOrganizerAuthorization(session);
        } catch (AuthenticationFailureException e) {
            return "redirect:/auth/login";
        } catch (UnauthorizedOperationException e) {
            model.addAttribute("error", e.getMessage());
            return "access-denied";
        }
        model.addAttribute("coverPhotos", photoService.getCoverPhotos());
        model.addAttribute("eligibleJuries", userService.getAllEligibleForJury());
        model.addAttribute("categories", categoryService.getAll());
        model.addAttribute("contest", new ContestDto());
        model.addAttribute("loggedUser", loggedUser);
        return "contest-new";
    }

    @PostMapping("/new")
    public String createContest(@RequestParam("file") MultipartFile file,
                                @Valid @ModelAttribute("contest") ContestDto contestDto, BindingResult errors,
                                Model model, HttpSession session) {
        User loggedUser;
        try {
            loggedUser = authenticationHelper.verifyOrganizerAuthorization(session);
        } catch (AuthenticationFailureException e) {
            return "redirect:/auth/login";
        } catch (UnauthorizedOperationException e) {
            model.addAttribute("error", e.getMessage());
            return "access-denied";
        }
        if (errors.hasErrors()) {
            model.addAttribute("error", errors.getAllErrors().get(0).getDefaultMessage());
            return "validation-error";
        }
        Contest contest = contestMapper.dtoToObject(contestDto);
        if (!file.isEmpty() || contestDto.getPhotoUrl() != null) {
            Photo photo;
            if (!file.isEmpty()) {
                photo = photoMapper.dtoToObjectCover(file);
            } else {
                photo = photoMapper.dtoToObjectCover(contestDto);
            }
            photoService.create(photo);
            contest.setCoverPhoto(photo);
        }
        try {
            contestService.create(contest, loggedUser);
            String redirect = "redirect:/contests/" + contestService.getByTitle(contest.getTitle()).getId();
            return redirect;
        } catch (UnauthorizedOperationException e) {
            model.addAttribute("error", e.getMessage());
            return "access-denied";
        } catch (DuplicateEntityException e) {
            model.addAttribute("error", e.getMessage());
            return "validation-error";
        }
    }

    @GetMapping("/{id}/entries")
    public String showAllEntriesForContest(@PathVariable int id, Model model, HttpSession session) {
        User loggedUser;
        try {
            loggedUser = authenticationHelper.tryGetUser(session);
        } catch (AuthenticationFailureException e) {
            return "redirect:/auth/login";
        }
        model.addAttribute("loggedUser", loggedUser);
        try {
            Contest contest = contestService.getById(id);
            List<Entry> entries = new ArrayList<>();
            if (populateIsOrganizer(session)) {
                entries = entryService.getAllByContestId(id);
            } else {
                Entry entry = entryService.getByUserAndContestId(loggedUser, id);
                entries.add(entry);
            }
            model.addAttribute("contest", contest);
            model.addAttribute("entries", entries);
            model.addAttribute("timeDifference", contestService.calculatePhaseTimeDiff(contest));
            model.addAttribute("contests", contestService.getAllPhaseOne(0,loggedUser));
            return "entries-contest";
        } catch (EntityNotFoundException e) {
            model.addAttribute("error", e.getMessage());
            return "404";
        }
    }

    @GetMapping("/{id}/entries/new")
    public String showNewEntryPage(@PathVariable int id, Model model, HttpSession session) {
        User loggedUser;
        try {
            loggedUser = authenticationHelper.tryGetUser(session);
        } catch (AuthenticationFailureException e) {
            return "redirect:/auth/login";
        }
        model.addAttribute("loggedUser", loggedUser);
        try {
            entryService.getByUserAndContestId(loggedUser, id);
            return "access-denied";
        } catch (EntityNotFoundException e) {
            model.addAttribute("entry", new EntryDto());
            return "entry-new";
        }
    }

    @PostMapping("/{id}/entries/new")
    public String createEntry(@PathVariable int id, @RequestParam("file") MultipartFile file,
                              RedirectAttributes redirectAttributes, @Valid @ModelAttribute("entry") EntryDto entryDto,
                              BindingResult errors, Model model, HttpSession session) {
        User loggedUser;
        try {
            loggedUser = authenticationHelper.tryGetUser(session);
        } catch (AuthenticationFailureException e) {
            return "redirect:/auth/login";
        }
        model.addAttribute("loggedUser", loggedUser);
        if (file.isEmpty()) {
            redirectAttributes.addFlashAttribute("message", PLEASE_SELECT_A_FILE_TO_UPLOAD);
            return "redirect:/contests/{id}/entries/new";
        }
        if (errors.hasErrors()) {
            List<ObjectError> err = errors.getAllErrors();
            String err1 = err.get(0).getDefaultMessage();
            redirectAttributes.addFlashAttribute("error1", err1);
            if (err.size() == 2) {
                String err2 = err.get(1).getDefaultMessage();
                redirectAttributes.addFlashAttribute("error2", err2);
            }
            return "redirect:/contests/{id}/entries/new";
        }
        try {
            Photo photo = photoMapper.dtoToObjectUpload(file);
            photoService.create(photo);
            Entry entry = entryMapper.dtoToObject(entryDto, photo, id, loggedUser);
            entryService.create(entry, loggedUser);
            return "redirect:/contests/{id}";
        } catch (DuplicateEntityException e) {
            errors.rejectValue("title", "duplicate_entry", e.getMessage());
            return "entry-new";
        } catch (EntityNotFoundException e) {
            model.addAttribute("error", e.getMessage());
            return "404";
        } catch (UnauthorizedOperationException e) {
            model.addAttribute("error", e.getMessage());
            return "access-denied";
        }
    }

    @GetMapping("/{id}/invites/new")
    public String showNewInvitePage(@PathVariable int id, Model model, HttpSession session) {
        User loggedUser;
        try {
            loggedUser = authenticationHelper.verifyOrganizerAuthorization(session);
        } catch (AuthenticationFailureException e) {
            return "redirect:/auth/login";
        } catch (UnauthorizedOperationException e) {
            return "access-denied";
        }
        model.addAttribute("contest", contestService.getById(id));
        model.addAttribute("invitedUsers", userService.getAvailableUsersForInvite(id));
        model.addAttribute("invite", new InviteDto());
        model.addAttribute("loggedUser", loggedUser);
        return "invite-new";
    }

    @PostMapping("/{id}/invites/new")
    public String createInvite(@PathVariable int id, @Valid @ModelAttribute("invite") InviteDto inviteDto,
                               BindingResult errors, Model model, HttpSession session) {
        User loggedUser;
        try {
            loggedUser = authenticationHelper.verifyOrganizerAuthorization(session);
        } catch (AuthenticationFailureException e) {
            return "redirect:/auth/login";
        } catch (UnauthorizedOperationException e) {
            model.addAttribute("error", e.getMessage());
            return "access-denied";
        }
        if (errors.hasErrors()) {
            model.addAttribute("error", errors.getAllErrors().get(0).getDefaultMessage());
            return "validation-error";
        }
        try {
            List<Invite> invites = inviteMapper.dtoToList(inviteDto, id, loggedUser.getId());
            if (invites.size() == 1) {
                inviteService.create(invites.get(0));
            } else {
                inviteService.massCreate(invites);
            }
            return "redirect:/invites";
        } catch (DuplicateEntityException e) {
            errors.rejectValue("title", "duplicate_entry", e.getMessage());
            return "invite-new";
        } catch (EntityNotFoundException e) {
            model.addAttribute("error", e.getMessage());
            return "404";
        }
    }

    @GetMapping("/one")
    public String showAllPhaseOneContests(Model model, HttpSession session) {
        User loggedUser;
        try {
           loggedUser = authenticationHelper.tryGetUser(session);
        } catch (AuthenticationFailureException e) {
            return "redirect:/auth/login";
        }
        model.addAttribute("loggedUser", loggedUser);
        model.addAttribute("contests", contestService.getAllPhaseOne(0,loggedUser));
        return "contests-phase-one";
    }

    @GetMapping("/two")
    public String showAllPhaseTwoContests(Model model, HttpSession session) {
        User loggedUser;
        try {
          loggedUser = authenticationHelper.tryGetUser(session);
        } catch (AuthenticationFailureException e) {
            return "redirect:/auth/login";
        }
        model.addAttribute("contests", contestService.getAllPhaseTwo(0,loggedUser));
        model.addAttribute("loggedUser", loggedUser);
        return "contests-phase-two";
    }

    @GetMapping("/finished")
    public String showAllPhaseFinishContests(Model model, HttpSession session) {
        User loggedUser;
        try {
            loggedUser = authenticationHelper.tryGetUser(session);
        } catch (AuthenticationFailureException e) {
            return "redirect:/auth/login";
        }
        model.addAttribute("contests", contestService.getAllPhaseFinished(0 ,loggedUser));
        model.addAttribute("loggedUser", loggedUser);
        return "contests-phase-finished";
    }

    @ModelAttribute("phases")
    public Phase[] fetchAllPhase() {
        return Phase.values();
    }

    @ModelAttribute("categories")
    public List<Category> populateCategories() {
        return categoryService.getAll();
    }

    @ModelAttribute("types")
    public List<Contest> populateContestTypes() {
        return contestService.getAll();
    }

    @PostMapping("/filter")
    public String filterContests(@ModelAttribute("filterContestDto") FilterContestDto filterContestDto,
                                 HttpSession session, Model model) {
        User user;
        try {
            user = authenticationHelper.verifyOrganizerAuthorization(session);
        } catch (AuthenticationFailureException e) {
            return "redirect:/auth/login";
        }

        var filtered = contestService.filter(
                Optional.ofNullable(filterContestDto.getTitle().isBlank() ? null : filterContestDto.getTitle()),
                Optional.ofNullable(filterContestDto.getOpenAccess().equals("") ? null : filterContestDto.getOpenAccess()),
                Optional.ofNullable(filterContestDto.getPhase().equals("") ?
                        null : Phase.valueOf(filterContestDto.getPhase().toUpperCase())),
                Optional.ofNullable(filterContestDto.getCategoryId() == null ? null : filterContestDto.getCategoryId())
        );
        model.addAttribute("contests", filtered);
        return "contests";
    }

    @GetMapping("/filter")
    public String showAllContests(Model model, HttpSession session) {
        User user;
        try {
            user = authenticationHelper.verifyOrganizerAuthorization(session);
            model.addAttribute("loggedUser", user);
        } catch (UnauthorizedOperationException e) {
            model.addAttribute("error", e.getMessage());
            return "access-denied";
        } catch (AuthenticationFailureException e) {
            return "redirect:/auth/login";
        }
        List<Contest> contests = contestService.getAll();
        model.addAttribute("contests", contests);
        model.addAttribute("filterContestDto", new FilterContestDto());
        model.addAttribute("loggedUser", user);
        return "contests";
    }

}
