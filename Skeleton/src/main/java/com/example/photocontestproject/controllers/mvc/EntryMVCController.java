package com.example.photocontestproject.controllers.mvc;

import com.example.photocontestproject.controllers.AuthenticationHelper;
import com.example.photocontestproject.exceptions.AuthenticationFailureException;
import com.example.photocontestproject.exceptions.EntityNotFoundException;
import com.example.photocontestproject.exceptions.InvalidContestPhaseException;
import com.example.photocontestproject.exceptions.UnauthorizedOperationException;
import com.example.photocontestproject.models.Contest;
import com.example.photocontestproject.models.Entry;
import com.example.photocontestproject.models.Review;
import com.example.photocontestproject.models.User;
import com.example.photocontestproject.models.dtos.ReviewDto;
import com.example.photocontestproject.services.contracts.ContestService;
import com.example.photocontestproject.services.contracts.EntryService;
import com.example.photocontestproject.services.contracts.ReviewService;
import com.example.photocontestproject.services.contracts.UserService;
import com.example.photocontestproject.utils.mappers.ReviewMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpSession;
import javax.validation.Valid;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import static com.example.photocontestproject.utils.constants.GlobalConstants.ORGANIZER_ROLE;

@Controller
@RequestMapping("/entries")
public class EntryMVCController {

    private final AuthenticationHelper authenticationHelper;
    private final ReviewMapper reviewMapper;
    private final ReviewService reviewService;
    private final ContestService contestService;
    private final UserService userService;
    private final EntryService entryService;

    @Autowired
    public EntryMVCController(AuthenticationHelper authenticationHelper,
                              ReviewMapper reviewMapper, ReviewService reviewService, ContestService contestService, UserService userService, EntryService entryService) {
        this.authenticationHelper = authenticationHelper;
        this.reviewMapper = reviewMapper;
        this.reviewService = reviewService;
        this.contestService = contestService;
        this.userService = userService;
        this.entryService = entryService;
    }

    @ModelAttribute("isAuthenticated")
    public boolean populateIsAuthenticated(HttpSession session) {
        return session.getAttribute("currentUser") != null;
    }

    @ModelAttribute("isOrganizer")
    public boolean populateIsOrganizer(HttpSession session) {
        User loggedUser;
        try {
            loggedUser = authenticationHelper.tryGetUser(session);
            return loggedUser.getRole().getRole().equals(ORGANIZER_ROLE);
        } catch (AuthenticationFailureException | EntityNotFoundException | NullPointerException e) {
            return false;
        }
    }

    @GetMapping()
    public String showAllEntries(Model model, HttpSession session) {
        User loggedUser;
        try {
            loggedUser = authenticationHelper.tryGetUser(session);
        } catch (AuthenticationFailureException e) {
            return "redirect:/auth/login";
        }
        try {
            List<Entry> entries;
            if (populateIsOrganizer(session)) {
                entries = entryService.getAllForOpenContests();
            } else {
                entries = entryService.getAllByUser(loggedUser);
            }
            model.addAttribute("loggedUser", loggedUser);
            model.addAttribute("entries", entries);
            return "entries";
        } catch (EntityNotFoundException e) {
            model.addAttribute("error", e.getMessage());
            return "404";
        }
    }

    @GetMapping("/{id}")
    public String showSingleEntry(@PathVariable int id, Model model, HttpSession session) {
        User loggedUser;
        try {
            loggedUser = authenticationHelper.tryGetUser(session);
        } catch (AuthenticationFailureException e) {
            return "redirect:/auth/login";
        }
        try {
            Entry entry = entryService.getById(id);
            model.addAttribute("loggedUser", loggedUser);
            model.addAttribute("entry", entry);
            model.addAttribute("isJury", userService.isUserJury(loggedUser, entryService.getById(id).getContest()));
            Contest contest = contestService.getById(entry.getContest().getId());

            LocalDateTime endPhase1 = contest.getPhaseOneExpiration();
            LocalDateTime endPhase2 = contest.getPhaseTwoExpiration();
            boolean canShow = endPhase1.isBefore(LocalDateTime.now()) && endPhase2.isAfter(LocalDateTime.now());
            model.addAttribute("canShow", canShow);
            model.addAttribute("review", new ReviewDto());

            if (contest.getPhaseTwoExpiration().isBefore(LocalDateTime.now())) {
                boolean canShowFinal = endPhase2.isBefore(LocalDateTime.now());
                double finalScore = reviewService.getEntryFinalScore(entry);
                finalScore = Math.round(finalScore * 100.0) / 100.0;
                model.addAttribute("finalScore", finalScore);
                model.addAttribute("canShowFinal", canShowFinal);
            }

            model.addAttribute("entryReview", reviewService.getByEntry(entry));
            return "entry";
        } catch (EntityNotFoundException e) {
            model.addAttribute("error", e.getMessage());
            return "404";
        } catch (InvalidContestPhaseException e) {
            model.addAttribute("error", e.getMessage());
            return "404";
        }
    }

    @GetMapping("/{id}/delete")
    public String deleteEntry(@PathVariable int id, Model model, HttpSession session) {
        User loggedUser;
        try {
            loggedUser = authenticationHelper.tryGetUser(session);
        } catch (AuthenticationFailureException e) {
            return "redirect:/auth/login";
        }
        model.addAttribute("loggedUser", loggedUser);
        try {
            entryService.delete(id, loggedUser);
            return "redirect:/entries";
        } catch (EntityNotFoundException e) {
            model.addAttribute("error", e.getMessage());
            return "404";
        } catch (UnauthorizedOperationException e) {
            model.addAttribute("error", e.getMessage());
            return "access-denied";
        }
    }

    @PostMapping("/{id}")
    public String createReview(@Valid @ModelAttribute("review") ReviewDto reviewDto,
                               @PathVariable int id, Model model, BindingResult errors, HttpSession session) {
        User loggedUser;
        try {
            loggedUser = authenticationHelper.tryGetUser(session);
        } catch (AuthenticationFailureException e) {
            return "redirect:/auth/login";
        }
        if (errors.hasErrors()) {
            return "entry";
        }
        try {
            reviewDto.setJuryId(loggedUser.getId());
            reviewDto.setEntryId(id);
            Review review = reviewMapper.fromDto(reviewDto);
            reviewService.create(review, loggedUser);
            return "redirect:/entries/" + id;
        } catch (EntityNotFoundException e) {
            model.addAttribute("error", e.getMessage());
            return "404";
        } catch (UnauthorizedOperationException e) {
            model.addAttribute("error", e.getMessage());
            return "access-denied";
        }
    }

    @PostMapping("/search")
    public String handleSearch(@ModelAttribute("search") Optional<String> search, HttpSession session, Model model) {
        User user;
        try {
            user = authenticationHelper.verifyOrganizerAuthorization(session);
        } catch (AuthenticationFailureException e) {
            return "redirect:/auth/login";
        }
        List<Entry> entries = new ArrayList<>();
        entries = entryService.search(search, user);
        model.addAttribute("entries", entries);
        return "entries-search";
    }

    @GetMapping("/search")
    public String showAllEntriesForSearch(HttpSession session, Model model) {
        User user;
        try {
            user = authenticationHelper.verifyOrganizerAuthorization(session);
            model.addAttribute("loggedUser", user);
        } catch (UnauthorizedOperationException e) {
            model.addAttribute("error", e.getMessage());
            return "access-denied";
        } catch (AuthenticationFailureException e) {
            return "redirect:/auth/login";
        }
        List<Entry> entries = new ArrayList<>();
        entries = entryService.getAll();
        String search = "search";
        model.addAttribute("entries", entries);
        model.addAttribute("search", Optional.of(search));
        return "entries-search";
    }
}
