package com.example.photocontestproject.controllers.mvc;

import com.example.photocontestproject.controllers.AuthenticationHelper;
import com.example.photocontestproject.exceptions.AuthenticationFailureException;
import com.example.photocontestproject.exceptions.EntityNotFoundException;
import com.example.photocontestproject.models.User;
import com.example.photocontestproject.services.contracts.InviteService;
import com.example.photocontestproject.services.contracts.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;

import javax.servlet.http.HttpSession;

import static com.example.photocontestproject.utils.constants.GlobalConstants.ORGANIZER_ROLE;

@Controller
@RequestMapping("/invites")
public class InviteMVCController {

    private final AuthenticationHelper authenticationHelper;
    private final InviteService inviteService;
    private final UserService userService;

    @Autowired
    public InviteMVCController(AuthenticationHelper authenticationHelper, InviteService inviteService, UserService userService) {
        this.authenticationHelper = authenticationHelper;
        this.inviteService = inviteService;
        this.userService = userService;
    }

    @ModelAttribute("isAuthenticated")
    public boolean populateIsAuthenticated(HttpSession session) {
        return session.getAttribute("currentUser") != null;
    }

    @ModelAttribute("isOrganizer")
    public boolean populateIsOrganizer(HttpSession session) {
        User loggedUser;
        try {
            loggedUser = authenticationHelper.tryGetUser(session);
            return loggedUser.getRole().getRole().equals(ORGANIZER_ROLE);
        } catch (AuthenticationFailureException | EntityNotFoundException | NullPointerException e) {
            return false;
        }
    }

    @GetMapping
    public String showAllPage(Model model, HttpSession session) {
        User loggedUser;
        try {
            loggedUser = authenticationHelper.tryGetUser(session);
        } catch (AuthenticationFailureException e) {
            return "redirect:/auth/login";
        }
        model.addAttribute("loggedUser", loggedUser);
        model.addAttribute("invites", inviteService.getAll());
        return "invites";
    }
}
