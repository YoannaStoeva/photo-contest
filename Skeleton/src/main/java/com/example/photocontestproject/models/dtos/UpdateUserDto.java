package com.example.photocontestproject.models.dtos;

import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

public class UpdateUserDto {

    @NotNull
    @Size(min = 2, max = 20, message = "First name must between 2 and 20 symbols long")
    private String firstName;

    @NotNull
    @Size(min = 2, max = 20, message = "Last name must between 2 and 20 symbols long")
    private String lastName;

    @NotNull
    @Size(min = 4, max = 50, message = "The password must between 4 and 50 symbols long")
    private String password;

    public UpdateUserDto() {
    }

    public UpdateUserDto(String firstName, String lastName, String password) {
        this.firstName = firstName;
        this.lastName = lastName;
        this.password = password;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }
}
