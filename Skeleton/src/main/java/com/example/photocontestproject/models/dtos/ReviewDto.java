package com.example.photocontestproject.models.dtos;

import javax.validation.constraints.NotNull;

public class ReviewDto {

/*    @Range(min = 1, max = 10, message = "Score must be in the interval 0-10")*/
    private int score;

/*    @NotNull(message = "Comment field cannot be empty")
    @Size(min = 3, max = 1000, message = "Comment must be between 3 and 1000 symbols.")*/
    private String comment;

    @NotNull
    private int juryId;

    private boolean wrongCategory;

    @NotNull
    private int entryId;

    public ReviewDto() {
    }

    public int getScore() {
        return score;
    }

    public void setScore(int score) {
        this.score = score;
    }

    public String getComment() {
        return comment;
    }

    public void setComment(String comment) {
        this.comment = comment;
    }

    public int getJuryId() {
        return juryId;
    }

    public void setJuryId(int juryId) {
        this.juryId = juryId;
    }

    public boolean isWrongCategory() {
        return wrongCategory;
    }

    public void setWrongCategory(boolean wrongCategory) {
        this.wrongCategory = wrongCategory;
    }

    public int getEntryId() {
        return entryId;
    }

    public void setEntryId(int entryId) {
        this.entryId = entryId;
    }
}
