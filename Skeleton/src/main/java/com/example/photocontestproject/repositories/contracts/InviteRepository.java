package com.example.photocontestproject.repositories.contracts;

import com.example.photocontestproject.models.Contest;
import com.example.photocontestproject.models.Invite;
import com.example.photocontestproject.models.User;

import java.util.List;

public interface InviteRepository extends BaseCRUDRepository<Invite>{
    List<Invite> getAllByContestId(int id);

    Invite getByContestAndReceiver(Contest contest, User receiver);
}
