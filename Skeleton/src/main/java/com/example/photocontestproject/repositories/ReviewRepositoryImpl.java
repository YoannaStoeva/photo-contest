package com.example.photocontestproject.repositories;

import com.example.photocontestproject.exceptions.EntityNotFoundException;
import com.example.photocontestproject.models.Entry;
import com.example.photocontestproject.models.Review;
import com.example.photocontestproject.repositories.contracts.ReviewRepository;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.query.Query;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public class ReviewRepositoryImpl extends AbstractCRUDRepository<Review> implements ReviewRepository {

    private final SessionFactory sessionFactory;

    @Autowired
    public ReviewRepositoryImpl(SessionFactory sessionFactory) {
        super(Review.class, sessionFactory);
        this.sessionFactory = sessionFactory;
    }

    @Override
    public int getCountAllReviews() {
        try (Session session = sessionFactory.openSession()) {
            Query<Long> query = session.createQuery("select count(*) from Review r", Long.class);
            return Integer.parseInt(String.valueOf(query.uniqueResult()));
        }
    }

    @Override
    public List<Review> getByEntry(Entry entry) {
        try (Session session = sessionFactory.openSession()) {
            Query<Review> query = session.createQuery("from Review where entry = :entry", Review.class);
            query.setParameter("entry", entry);

            List<Review> result = query.list();
            if (result.size() == 0) {
                throw new EntityNotFoundException("There is no review to this entry");
            }
            return result;
        }
    }

    @Override
    public List<Review> getByEntry(int entryId) {
        try (Session session = sessionFactory.openSession()) {
            Query<Review> query = session.createQuery("from Review where entry.id = :entryId", Review.class);
            query.setParameter("entryId", entryId);
            List<Review> result = query.list();
            return result;
        }
    }

    @Override
    public Double avgEntryScore(Entry entry) {
        try (Session session = sessionFactory.openSession()) {
            Query<Double> query = session.createQuery("select avg(r.score) from Review r where entry = :entry",
                    Double.class);
            query.setParameter("entry", entry);
            List<Double> result = query.list();
            if (result.get(0) == null) result.set(0,0.00);
            return result.get(0);
        }
    }

    @Override
    public Review getByJurorAndByEntry(int jurorId, int entryId) {
        try (Session session = sessionFactory.openSession()) {
            Query<Review> query = session.createQuery("from Review r where r.entry.id = :entryId and r.juror.id = :jurorId", Review.class);
            query.setParameter("entryId", entryId);
            query.setParameter("jurorId", jurorId);

            List<Review> result = query.list();
            if (result.size() == 0) {
                throw new EntityNotFoundException("There is no review by this user");
            }
            return result.get(0);
        }
    }

    @Override
    public int getEntryReviewCount(int entryId) {
        try (Session session = sessionFactory.openSession()) {
            Query<Review> query = session.createQuery("from Review where entry.id = :entryId", Review.class);
            query.setParameter("entryId", entryId);
            return query.getResultList().size();
        }
    }
}
