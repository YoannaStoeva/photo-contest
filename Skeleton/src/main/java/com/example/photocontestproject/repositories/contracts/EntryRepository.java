package com.example.photocontestproject.repositories.contracts;

import com.example.photocontestproject.models.Entry;
import com.example.photocontestproject.models.Review;
import com.example.photocontestproject.models.User;

import java.util.List;
import java.util.Optional;

public interface EntryRepository extends BaseCRUDRepository<Entry> {

    List<Entry> getAllByContestId(int id);

    int getCountAllEntries();

    List<Entry> getAllByUser(User user);

    Entry getByUserAndContestId(User user, int contestId);

    int getCountByContestId(int id);

    List<Entry> getAllForOpenContests();

    List<Review> getEntryReviews(Entry entry);

    List<Entry> search(Optional<String> search);
}
