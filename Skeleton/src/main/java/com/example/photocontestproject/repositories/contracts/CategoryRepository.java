package com.example.photocontestproject.repositories.contracts;

import com.example.photocontestproject.models.Category;

public interface CategoryRepository extends BaseCRUDRepository<Category>{
}
