package com.example.photocontestproject.repositories.contracts;

import com.example.photocontestproject.models.Entry;
import com.example.photocontestproject.models.Review;

import java.util.List;

public interface ReviewRepository extends BaseCRUDRepository<Review> {

    int getCountAllReviews();

    List<Review> getByEntry(Entry entry);

    List<Review> getByEntry(int entryId);

    Double avgEntryScore(Entry entry);

    Review getByJurorAndByEntry(int jurorId, int entryId);

    int getEntryReviewCount(int entryId);
}
