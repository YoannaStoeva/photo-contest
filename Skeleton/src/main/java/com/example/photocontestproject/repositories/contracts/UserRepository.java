package com.example.photocontestproject.repositories.contracts;

import com.example.photocontestproject.models.Contest;
import com.example.photocontestproject.models.Entry;
import com.example.photocontestproject.models.Invite;
import com.example.photocontestproject.models.User;
import com.example.photocontestproject.models.enums.Ranking;
import com.example.photocontestproject.models.enums.UserSortOptions;

import java.util.List;
import java.util.Optional;

public interface UserRepository extends BaseCRUDRepository<User> {

    List<User> getAllOrganizers();

    int getCountAllRegularUsers();

    List<User> getAvailableUsersForInvite(int id);

    List<User> getAllEligibleForJury();

    List<User> getContestJury(Contest contest);

    List<User> search(Optional<String> search);

    List<User> filter(Optional<String> name, Optional<Ranking> ranking, Optional<UserSortOptions> sort);

    List<Entry> getAllEntries(User user);

    List<Invite> getAllInvites(User user);
}
