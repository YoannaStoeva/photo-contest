package com.example.photocontestproject.repositories;

import com.example.photocontestproject.models.Role;
import com.example.photocontestproject.models.User;
import com.example.photocontestproject.repositories.contracts.RoleRepository;
import org.hibernate.SessionFactory;
import org.springframework.stereotype.Repository;

@Repository
public class RoleRepositoryImpl extends AbstractReadRepository<Role> implements RoleRepository {

    public RoleRepositoryImpl(SessionFactory sessionFactory) {
        super(Role.class, sessionFactory);
    }

    @Override
    public Role getById(int id) {
        return super.getById(id);
    }

    @Override
    public Role getByRole(String role) {
        return super.getByField("role", role);
    }
}
