package com.example.photocontestproject.repositories;

import com.example.photocontestproject.exceptions.EntityNotFoundException;
import com.example.photocontestproject.models.Contest;
import com.example.photocontestproject.models.Invite;
import com.example.photocontestproject.models.User;
import com.example.photocontestproject.repositories.contracts.InviteRepository;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.query.NativeQuery;
import org.hibernate.query.Query;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public class InviteRepositoryImpl extends AbstractCRUDRepository<Invite> implements InviteRepository {

    private final SessionFactory sessionFactory;

    @Autowired
    public InviteRepositoryImpl(SessionFactory sessionFactory) {
        super(Invite.class, sessionFactory);
        this.sessionFactory = sessionFactory;
    }

    @Override
    public List<Invite> getAllByContestId(int id) {
        try (Session session = sessionFactory.openSession()) {
            Query<Invite> query = session.createQuery(
                    "select distinct i from Invite i " + "join i.contest  c where c.id = :id", Invite.class);
            query.setParameter("id", id);

            List<Invite> result = query.list();
            if (result.size() == 0) {
                throw new EntityNotFoundException("Invite", "contestId", String.valueOf(id));
            }
            return result;
        }
    }

    @Override
    public Invite getByContestAndReceiver(Contest contest, User receiver) {
        try (Session session = sessionFactory.openSession()) {
            NativeQuery<Invite> query = session.createNativeQuery(
                    "select cu.invite_id, cu.contest_id, cu.sender_user_id, cu.receiver_id " +
                            "from photo.contests_users cu " +
                            "join photo.contests c on c.contest_id = cu.contest_id " +
                            "join users u on u.user_id = cu.receiver_id " +
                            "where cu.contest_id = :contestId and cu.receiver_id = :receiverId");
            query.setParameter("contestId", contest.getId());
            query.setParameter("receiverId", receiver.getId());
            query.addEntity(Invite.class);
            List<Invite> result = query.list();
            if (result.size() == 0) {
                throw new EntityNotFoundException("User", "contestId", String.valueOf(contest.getId()));
            }
            return result.get(0);
        }
    }
}
