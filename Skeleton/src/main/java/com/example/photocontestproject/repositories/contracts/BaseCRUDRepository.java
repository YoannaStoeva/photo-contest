package com.example.photocontestproject.repositories.contracts;

public interface BaseCRUDRepository<T> extends BaseReadRepository<T> {

    void delete(int id);

    void create(T entity);

    void update(T entity);
}
