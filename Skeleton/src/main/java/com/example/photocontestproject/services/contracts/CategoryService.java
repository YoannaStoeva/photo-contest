package com.example.photocontestproject.services.contracts;

import com.example.photocontestproject.models.Category;
import com.example.photocontestproject.models.Contest;

import java.util.List;

public interface CategoryService {

    List<Category> getAll();

    Category getById(int id);

    Category getByName(String name);

    void create(Category category);

}
