package com.example.photocontestproject.services;

import com.example.photocontestproject.exceptions.EntityNotFoundException;
import com.example.photocontestproject.models.Contest;
import com.example.photocontestproject.models.Invite;
import com.example.photocontestproject.models.User;
import com.example.photocontestproject.repositories.contracts.InviteRepository;
import com.example.photocontestproject.services.contracts.InviteService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

import static com.example.photocontestproject.utils.constants.GlobalConstants.ORGANIZER_ROLE;

@Service
public class InviteServiceImpl implements InviteService {

    private final InviteRepository inviteRepository;

    @Autowired
    public InviteServiceImpl(InviteRepository inviteRepository) {
        this.inviteRepository = inviteRepository;
    }


    @Override
    public Invite getById(int id) {
        try {
            return inviteRepository.getById(id);
        } catch (EntityNotFoundException e) {
            throw new EntityNotFoundException(e.getMessage());
        }
    }

    @Override
    public List<Invite> getAll() {
        try {
            return inviteRepository.getAll();
        } catch (EntityNotFoundException e) {
            return new ArrayList<>();
        }
    }

    @Override
    public void create(Invite invite) {
        try {
            inviteRepository.getByContestAndReceiver(invite.getContest(), invite.getReceiver());
        } catch (EntityNotFoundException e) {
            inviteRepository.create(invite);
        }
    }

    @Override
    public void massCreate(List<Invite> invites) {
        for (Invite invite : invites) {
            try {
                inviteRepository.getByContestAndReceiver(invite.getContest(), invite.getReceiver());
            } catch (EntityNotFoundException e) {
                inviteRepository.create(invite);
            }
        }
    }

    @Override
    public void delete(Invite invite) {
        inviteRepository.delete(invite.getId());
    }

    @Override
    public boolean isUserInvitedOrOrganizer(User loggedUser, Contest contest) {
        boolean isInvited = true;
        try {
            inviteRepository.getByContestAndReceiver(contest, loggedUser);
        } catch (EntityNotFoundException e) {
            isInvited = false;
        }
        return loggedUser.getRole().getRole().equals(ORGANIZER_ROLE) || isInvited;
    }
}
