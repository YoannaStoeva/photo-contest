package com.example.photocontestproject.services.contracts;

import com.example.photocontestproject.models.Entry;
import com.example.photocontestproject.models.Review;
import com.example.photocontestproject.models.User;

import java.util.List;

public interface ReviewService {

    List<Review> getAll();

    Review getById(int id);

    List<Review> getByEntry(Entry entry);

    int getCountAllReviews();

    void create(Review review, User juror);

    double getEntryFinalScore(Entry entry);
}
