package com.example.photocontestproject.services;

import com.example.photocontestproject.exceptions.EntityNotFoundException;
import com.example.photocontestproject.exceptions.DuplicateEntityException;
import com.example.photocontestproject.exceptions.UnauthorizedOperationException;
import com.example.photocontestproject.models.*;
import com.example.photocontestproject.models.enums.Ranking;
import com.example.photocontestproject.models.enums.UserSortOptions;
import com.example.photocontestproject.repositories.contracts.InviteRepository;
import com.example.photocontestproject.repositories.contracts.RoleRepository;
import com.example.photocontestproject.repositories.contracts.UserRepository;
import com.example.photocontestproject.services.contracts.EntryService;
import com.example.photocontestproject.services.contracts.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import static com.example.photocontestproject.utils.constants.GlobalConstants.*;

@Service
public class UserServiceImpl implements UserService {

    private final UserRepository userRepository;
    private final RoleRepository roleRepository;
    private final InviteRepository inviteRepository;
    private final EntryService entryService;


    @Autowired
    public UserServiceImpl(UserRepository userRepository, RoleRepository roleRepository,
                           InviteRepository inviteRepository, EntryService entryService) {
        this.userRepository = userRepository;
        this.roleRepository = roleRepository;
        this.inviteRepository = inviteRepository;
        this.entryService = entryService;
    }

    @Override
    public List<User> getAll(User loggedUser) {
        checkAuthorization(loggedUser, INFORMATION_ERROR_MESSAGE);
        try {
            return userRepository.getAll();
        } catch (EntityNotFoundException e) {
            return new ArrayList<>();
        }
    }

    @Override
    public int getCountAllRegularUsers() {
        return userRepository.getCountAllRegularUsers();
    }

    @Override
    public User getById(int id, User loggedUser) {
        checkUserAndOrganizerAuthorization(loggedUser, id, USER_VIEW_ERROR_MESSAGE);
        try {
            return userRepository.getById(id);
        } catch (EntityNotFoundException e) {
            throw new EntityNotFoundException(e.getMessage());
        }
    }

    @Override
    public User getById(int id) {
        try {
            return userRepository.getById(id);
        } catch (EntityNotFoundException e) {
            throw new EntityNotFoundException(e.getMessage());
        }
    }

    @Override
    public User getByUsername(String username) {
        try {
            return userRepository.getByField("username", username);
        } catch (EntityNotFoundException e) {
            throw new EntityNotFoundException(e.getMessage());
        }
    }

    @Override
    public List<User> getAllOrganizers() {
        try {
            return userRepository.getAllOrganizers();
        } catch (EntityNotFoundException e) {
            return new ArrayList<>();
        }
    }

    @Override
    public List<User> getAvailableUsersForInvite(int id) {
        try {
            return userRepository.getAvailableUsersForInvite(id);
        } catch (EntityNotFoundException e) {
            return new ArrayList<>();
        }
    }

    @Override
    public List<User> getAllEligibleForJury() {
        try {
            return userRepository.getAllEligibleForJury();
        } catch (EntityNotFoundException e) {
            return new ArrayList<>();
        }
    }

    @Override
    public List<User> getContestJury(Contest contest) {
        try {
            return userRepository.getContestJury(contest);
        } catch (EntityNotFoundException e) {
            return new ArrayList<>();
        }
    }

    @Override
    public boolean isUserJury(User user, Contest contest) {
        List<User> jury = userRepository.getContestJury(contest);
        return jury.contains(user);
    }

    @Override
    public List<User> search(Optional<String> search, User loggedUser) {
        checkAuthorization(loggedUser, ORGANIZER_SEARCH_ERROR_MESSAGE);
        return userRepository.search(search);
    }

    @Override
    public List<User> filter(Optional<String> name, Optional<Ranking> ranking, Optional<UserSortOptions> sort) {
        return userRepository.filter(name, ranking, sort);
    }

    @Override
    public void create(User user) {
        checkDuplicate(user);
        user.setCreationTime(LocalDateTime.now());
        userRepository.create(user);
    }

    @Override
    public void update(User userToUpdate, User loggedUser) {
        if (loggedUser.getId() != userToUpdate.getId()) {
            throw new UnauthorizedOperationException(USER_EDIT_ERROR_MESSAGE);
        }
        userRepository.update(userToUpdate);
    }

    @Override
    public User promote(User loggedUser, User userToPromote) {
        checkAuthorization(loggedUser, ORGANIZER_PROMOTE_ERROR_MESSAGE);
        userToPromote.setRole(roleRepository.getByRole(ORGANIZER_ROLE));
        userRepository.update(userToPromote);
        return userToPromote;
    }

    @Override
    public void delete(int id, User loggedUser) {
        checkAuthorization(loggedUser, ORGANIZER_DELETE_ERROR_MESSAGE);
        User userToBeDeleted = userRepository.getById(id);
        if (userToBeDeleted.getRole().getRole().equals(ORGANIZER_ROLE)) {
            throw new UnauthorizedOperationException(ORGANIZER_NOT_TO_BE_DELETED_ERROR_MESSAGE);
        }
        List<Invite> invites = userRepository.getAllInvites(userToBeDeleted);
        if (!invites.isEmpty()) {
            for (Invite invite : invites) {
                inviteRepository.delete(invite.getId());
            }
        }
        List<Entry> entries = userRepository.getAllEntries(userToBeDeleted);
        if (!entries.isEmpty()) {
            for (Entry entry : entries) {
                entryService.delete(entry.getId(), loggedUser);
            }
        }
        userRepository.delete(id);
    }

    private void checkDuplicate(User user) {
        boolean duplicateExists = true;
        try {
            userRepository.getByField("Username", user.getUsername());
        } catch (EntityNotFoundException e) {
            duplicateExists = false;
        }
        if (duplicateExists) {
            throw new DuplicateEntityException("User", "username", user.getUsername());
        }
        duplicateExists = true;
        try {
            userRepository.getByField("Email", user.getEmail());
        } catch (EntityNotFoundException e) {
            duplicateExists = false;
        }
        if (duplicateExists) {
            throw new DuplicateEntityException("User", "email", user.getEmail());
        }
    }

    private void checkAuthorization(User loggedUser, String message) {
        if (!loggedUser.getRole().getRole().equals(ORGANIZER_ROLE)) {
            throw new UnauthorizedOperationException(message);
        }
    }

    private void checkUserAndOrganizerAuthorization(User loggedUser, int id, String message) {
        if (!loggedUser.getRole().getRole().equals(ORGANIZER_ROLE) && loggedUser.getId() != id)
            throw new UnauthorizedOperationException(message);
    }
}
