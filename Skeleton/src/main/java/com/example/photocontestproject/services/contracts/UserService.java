package com.example.photocontestproject.services.contracts;

import com.example.photocontestproject.models.Contest;
import com.example.photocontestproject.models.User;
import com.example.photocontestproject.models.enums.Ranking;
import com.example.photocontestproject.models.enums.UserSortOptions;

import java.util.List;
import java.util.Optional;

public interface UserService {

    List<User> getAll(User loggedUser);

    User getById(int id, User loggedUser);

    User getById(int id);

    User getByUsername(String username);

    int getCountAllRegularUsers();

    List<User> getAllOrganizers();

    List<User> getAvailableUsersForInvite(int id);

    List<User> getAllEligibleForJury();

    List<User> getContestJury(Contest contest);

    boolean isUserJury(User user, Contest contest);

    List<User> search(Optional<String> search, User loggedUser);

    List<User> filter(Optional<String> name, Optional<Ranking> ranking, Optional<UserSortOptions> sort);

    void create(User user);

    void update(User user, User loggedUser);

    User promote(User loggedUser, User userToPromote);

    void delete(int id, User loggedUser);
}
