package com.example.photocontestproject.utils.mappers;

import com.example.photocontestproject.exceptions.EntityNotFoundException;
import com.example.photocontestproject.models.Photo;
import com.example.photocontestproject.models.dtos.ContestDto;
import com.example.photocontestproject.models.dtos.PhotoDto;
import com.example.photocontestproject.services.contracts.PhotoService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.web.multipart.MultipartFile;

import java.io.IOException;
import java.util.Optional;

import static com.example.photocontestproject.utils.constants.GlobalConstants.READ_FOLDER_COVER;
import static com.example.photocontestproject.utils.constants.GlobalConstants.UPLOAD_FOLDER;

@Component
public class PhotoMapper {

    private final PhotoService photoService;

    @Autowired
    public PhotoMapper(PhotoService photoService) {
        this.photoService = photoService;
    }

    public Photo dtoToObjectWithPath(PhotoDto photoDto) {
        Photo photo = new Photo();
        try {
            photo = photoService.getByPath(photoDto.getPathOrUrl());
        } catch (EntityNotFoundException e) {
            photo.setPath(photoDto.getPathOrUrl());
        }
        photo.setCoverPhoto(false);
        photo.setUrl(false);
        return photo;
    }

    public Photo dtoToObjectUpload(MultipartFile file) {
        try {
            photoService.writeFile(file,false);
        } catch (IOException e) {
            e.printStackTrace();
        }
        PhotoDto photoDto = new PhotoDto();
        photoDto.setPathOrUrl(UPLOAD_FOLDER + file.getOriginalFilename());
        return dtoToObjectWithPath(photoDto);
    }

    public Photo dtoToObjectUpload(MultipartFile file,int photoId) {
        try {
            photoService.writeFile(file,false);
        } catch (IOException e) {
            e.printStackTrace();
        }
        PhotoDto photoDto = new PhotoDto();
        photoDto.setPathOrUrl(UPLOAD_FOLDER + file.getOriginalFilename());
        Photo photo = dtoToObjectWithPath(photoDto);
        photo.setId(photoId);
        return photo;
    }

    public Photo dtoToObjectCover(MultipartFile file) {
        PhotoDto photoDto = new PhotoDto();
        try {
            photoService.writeFile(file,true);
            photoDto.setPathOrUrl(READ_FOLDER_COVER + file.getOriginalFilename());
        } catch (IOException e) {
            e.printStackTrace();
        }
        Photo photo = dtoToObjectWithPath(photoDto);
        photo.setCoverPhoto(true);
        return photo;
    }

    public Photo dtoToObjectCover(Optional<String> url) {

        PhotoDto photoDto = new PhotoDto();
        photoDto.setPathOrUrl(url.toString());
        Photo photo = dtoToObjectWithPath(photoDto);
        photo.setCoverPhoto(true);
        photo.setUrl(true);
        return photo;
    }

    public Photo dtoToObjectCoverPhotoId(Optional<Integer> photoId) {
        try{
           return photoService.getById(photoId.get());
        } catch (EntityNotFoundException e) {
            throw new EntityNotFoundException(e.getMessage());
        }
    }

    public Photo dtoToObjectCover(ContestDto contestDto) {
        PhotoDto photoDto = new PhotoDto();
        if (contestDto.getPhotoUrl() != null && contestDto.getPhotoUrl().contains(READ_FOLDER_COVER)) {
                contestDto.setPhotoUrl(contestDto.getPhotoUrl().substring(1));
        }
        photoDto.setPathOrUrl(contestDto.getPhotoUrl());
        Photo photo = dtoToObjectWithPath(photoDto);
        photo.setCoverPhoto(true);
        if (!photo.getPath().contains(READ_FOLDER_COVER)) photo.setUrl(true);
        return photo;
    }
}
