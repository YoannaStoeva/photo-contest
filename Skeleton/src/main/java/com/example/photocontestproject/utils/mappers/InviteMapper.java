package com.example.photocontestproject.utils.mappers;

import com.example.photocontestproject.models.Invite;
import com.example.photocontestproject.models.dtos.InviteDto;
import com.example.photocontestproject.services.contracts.ContestService;
import com.example.photocontestproject.services.contracts.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.List;

@Component
public class InviteMapper {

    private final UserService userService;
    private final ContestService contestService;

    @Autowired
    public InviteMapper(UserService userService, ContestService contestService) {
        this.userService = userService;
        this.contestService = contestService;
    }

    public List<Invite> dtoToList(InviteDto inviteDto,int contestId, int userId) {
        List<Invite> invites = new ArrayList<>();
        if (inviteDto.getReceiverIds() != null) {
            for (int receiver : inviteDto.getReceiverIds()) {
                Invite invite = new Invite();
                invite.setContest(contestService.getById(contestId));
                invite.setSender(userService.getById(userId));
                invite.setReceiver(userService.getById(receiver));
                invites.add(invite);
            }
        }
        return invites;
    }
}
