package com.example.photocontestproject.utils;

import com.example.photocontestproject.services.contracts.ContestService;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;

@Component
public class ScheduledTasks {

    private final ContestService contestService;

    public ScheduledTasks(ContestService contestService) {
        this.contestService = contestService;
    }

//    @Scheduled(cron = "0 0/15 * * * ?")
    @Scheduled(cron = "0/15 * * * * ?")
    public void progressPhase(){
        contestService.progressPhase();
    }

}
