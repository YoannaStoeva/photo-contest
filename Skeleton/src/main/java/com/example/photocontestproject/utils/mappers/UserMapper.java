package com.example.photocontestproject.utils.mappers;

import com.example.photocontestproject.models.Role;
import com.example.photocontestproject.models.User;
import com.example.photocontestproject.models.dtos.RegisterDto;
import com.example.photocontestproject.models.dtos.UpdateUserDto;
import com.example.photocontestproject.models.dtos.UserDto;
import com.example.photocontestproject.models.enums.Ranking;
import com.example.photocontestproject.repositories.contracts.RoleRepository;
import com.example.photocontestproject.repositories.contracts.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import static com.example.photocontestproject.utils.constants.GlobalConstants.USER_ROLE;

@Component
public class UserMapper {

    private final UserRepository userRepository;
    private final RoleRepository roleRepository;

    @Autowired
    public UserMapper(UserRepository userRepository, RoleRepository roleRepository) {
        this.userRepository = userRepository;
        this.roleRepository = roleRepository;
    }

    public User userFromDto(UserDto userDto) {
        User user = new User();
        userDtoToObject(userDto, user);
        return user;
    }

    public User userFromDto(UpdateUserDto updateUserDto, int id) {
        User user = userRepository.getById(id);
        userDtoToObject(updateUserDto, user);
        return user;
    }

    public UpdateUserDto fromUser(User user) {
        UpdateUserDto updateUserDto = new UpdateUserDto();
        updateUserDto.setFirstName(user.getFirstName());
        updateUserDto.setLastName(user.getLastName());
        return updateUserDto;
    }

    public User dtoToObject(RegisterDto registerDto) {
        User user = new User();
        Role role = roleRepository.getByRole(USER_ROLE);
        user.setFirstName(registerDto.getFirstName());
        user.setLastName(registerDto.getLastName());
        user.setEmail(registerDto.getEmail());
        user.setUsername(registerDto.getUsername());
        user.setPassword(registerDto.getPassword());
        user.setRole(role);
        user.setRanking(Ranking.JUNKIE);
        return user;
    }

    public void userDtoToObject(UserDto userDto, User user) {
        Role role = roleRepository.getByRole(USER_ROLE);
        user.setFirstName(userDto.getFirstName());
        user.setLastName(userDto.getLastName());
        user.setEmail(userDto.getEmail());
        user.setUsername(userDto.getUsername());
        user.setPassword(userDto.getPassword());
        user.setRole(role);
    }

    public void userDtoToObject(UpdateUserDto updateUserDto, User user) {
        user.setFirstName(updateUserDto.getFirstName());
        user.setLastName(updateUserDto.getLastName());
        user.setPassword(updateUserDto.getPassword());
    }
}

