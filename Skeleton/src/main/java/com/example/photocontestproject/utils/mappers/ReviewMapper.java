package com.example.photocontestproject.utils.mappers;

import com.example.photocontestproject.models.Review;
import com.example.photocontestproject.models.dtos.ReviewDto;
import com.example.photocontestproject.services.contracts.EntryService;
import com.example.photocontestproject.services.contracts.ReviewService;
import com.example.photocontestproject.services.contracts.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Component
public class ReviewMapper {

    private final ReviewService reviewService;
    private final UserService userService;
    private final EntryService entryService;

    @Autowired
    public ReviewMapper(ReviewService reviewService, UserService userService, EntryService entryService) {
        this.reviewService = reviewService;
        this.userService = userService;
        this.entryService = entryService;
    }

    public Review reviewDtoToReview(ReviewDto reviewDto, int id) {
        Review review = reviewService.getById(id);
        review.setScore(reviewDto.getScore());
        review.setComment(reviewDto.getComment());
        return review;
    }

    public Review fromDto(ReviewDto reviewDto) {
        Review review = new Review();
        dtoToObject(review,reviewDto);
        return review;
    }

    private void dtoToObject(Review review, ReviewDto reviewDto) {
        review.setJuror(userService.getById(reviewDto.getJuryId()));
        review.setEntry(entryService.getById(reviewDto.getEntryId()));
        if (reviewDto.isWrongCategory()) {
            review.setScore(0);
            review.setComment("The photo does not fit the contest category");
        } else {
            review.setScore(reviewDto.getScore());
            review.setComment(reviewDto.getComment());
        }
        review.setIsWrongCategory(reviewDto.isWrongCategory());
    }
}
