package com.example.photocontestproject.exceptions;

public class InvalidContestPhaseException extends RuntimeException {

    public InvalidContestPhaseException(String message) {
        super(message);
    }
}
